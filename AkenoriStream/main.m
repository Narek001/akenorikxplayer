//
//  main.m
//  AkenoriStream
//
//  Created by Dmitriy on 28.09.16.
//  Copyright © 2016 GrowApp Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

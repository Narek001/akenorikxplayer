//
//  ViewController.m
//  AkenoriStream
//
//  Created by Dmitriy on 28.09.16.
//  Copyright © 2016 GrowApp Solutions. All rights reserved.
//

#import "ViewController.h"
#import "KxMovieViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *playerContentView;
@property (nonatomic, strong) KxMovieViewController *controller;
@end

@implementation ViewController

-(void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
//    NSString *path = @"rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov";
    NSString *path  = @"rtsp://192.168.203.1:8554";
    _controller = [KxMovieViewController movieViewControllerWithContentPath: path parameters:nil];
    [self addChildViewController:_controller];
    [self.view addSubview:_controller.view];
    [_controller didMoveToParentViewController:self];
    
    CGRect rect = CGRectMake(0, 0, 100, 100);
    [_controller setPlayerFrame:rect];
    [_controller.view setFrame:rect];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect rect = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
    [_controller.view setFrame:rect];
}

- (IBAction)makePlayerFullscreen:(id)sender {
     CGRect screenRect = [[UIScreen mainScreen] bounds];
     CGRect rect = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height*0.5);
    [_controller.view setFrame:rect];
}


@end

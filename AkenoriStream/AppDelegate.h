//
//  AppDelegate.h
//  AkenoriStream
//
//  Created by Dmitriy on 28.09.16.
//  Copyright © 2016 GrowApp Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

